# Palindrome Checker Lambda Function

The code is a simple AWS Lambda function written in Rust that checks whether a given string is a palindrome. The function is triggered by an event containing a string to be checked. The result is returned in a response containing a status code and a message.

## Code Overview

- **Request and Response Structs:**
    - `Request`: Deserialized struct representing the input event payload.
    - `Response`: Serialized struct representing the function's output.

- **Palindrome Checking Function:**
    - `is_palindrome`: Function to determine if a string is a palindrome.

- **Handler Function:**
    - `function_handler`: Async function handling the Lambda event, checking if the input string is a palindrome, and generating a response.

- **Main Function:**
    - Sets up the tracing subscriber for logging.
    - Uses the `lambda_runtime` crate to run the Lambda function.

## AWS Deployment

To prepare for API Gateway integration, follow these steps:

1. **Create the AWS account:**<br>
Sign up for an AWS account if you haven't already.
2. **Generate AWS Access Keys:**<br>
Generate AWS Access Key ID and Secret Access Key for authentication.
3. **Configure AWS Credentials:**<br>
Use the following command to set up your AWS Access Key ID and Secret Access Key:
  ```shell
    aws configure
  ```
4. **Build the Project:**<br>
Build the project using the following command:
```shell
  cargo lambda build --release
```
5. **Deploy the Project as an AWS Lambda Function:**<br>
Deploy the project as an AWS Lambda Function with the following command:
```shell
  cargo lambda deploy
```
6. **Create a New REST API Gateway:**<br>
Set up a new REST API Gateway that connects to the Lambda Function deployed in the previous step.

7. **Deploy the API:**<br>
Deploy the API, specifying the desired stages for your application.
   
## Testing
Now that the lambda function is deployed and can be accessed through the following url:

<https://o674rmyeh1.execute-api.us-east-1.amazonaws.com/xiuyuan_resource>

Notice that `str` should be included in the request body. You can test it by sending a curl post request:

```shell
  curl -X POST https://o674rmyeh1.execute-api.us-east-1.amazonaws.com/xiuyuan_resource \
  -H 'content-type: application/json' \
  -d '{ "str": "abba"}'      
```

Alternatively, try the following codes to test the lambda function locally:

```shell
  cargo lambda watch
  cargo lambda invoke --data-ascii "{ \"str\": \"replace_it\" }"
```

## Results

The following screenshots show the outcomes of the code:

![Screenshot 2024-02-07 at 6.08.19 PM.png](screenshot%2FScreenshot%202024-02-07%20at%206.08.19%20PM.png)

![Screenshot 2024-02-07 at 6.08.57 PM.png](screenshot%2FScreenshot%202024-02-07%20at%206.08.57%20PM.png)

