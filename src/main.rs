use tracing_subscriber::filter::{EnvFilter, LevelFilter};
use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use serde::{Serialize, Deserialize};

#[derive(Deserialize)]
struct Request {
    str: String,
}

#[derive(Serialize)]
struct Response {
    #[serde(default = "default_status_code")]
    statusCode: u16,
    message: String,
}

fn default_status_code() -> u16 {
    400
}

fn is_palindrome(s: &str) -> bool {
    let s = s.chars().filter(|c| c.is_alphanumeric()).collect::<String>();
    let reversed = s.chars().rev().collect::<String>();
    s.eq_ignore_ascii_case(&reversed)
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    let input_str = event.payload.str.clone();

    let is_palindrome = is_palindrome(&input_str);

    let message = if is_palindrome {
        format!("'{}' is a palindrome!", input_str)
    } else {
        format!("'{}' is not a palindrome.", input_str)
    };

    let response = Response {
        statusCode: 200,
        message
    };

    Ok(response)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_env_filter(
            EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    run(service_fn(function_handler)).await
}
